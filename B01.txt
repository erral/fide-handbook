Approved by the 1982 General Assembly and amended by the General Assemblies of 1984 through 2012.

0.Introduction
0.1 Only the titles as in 0.3 are acknowledged by FIDE.
0.2 The following regulations can only be altered by the General Assembly following recommendation by the Qualification Commission (QC).
0.21 Any such changes shall only be made every fourth year, commencing from 2004 (unless the Commission agrees urgent action is required).
0.22 Any such changesshall take effect from 1July of the year following the decision by the General Assembly. For tournaments, such changes shall apply to those starting on or after that date.
0.3 The international FIDE titles for over the board play shall be under the umbrella of the QC, which is the final judging unit. The titles are:
0.31 Titles for over-the-board standard chess (as defined in 1.14), the judging unit being the QC:
Grandmaster (GM), International Master (IM), FIDE Master (FM), Candidate Master (CM), Woman Grandmaster (WGM), Woman International Master (WIM), Woman FIDE Master (WFM), Woman Candidate Master (WCM).



0.4 The titles are valid for life from the date confirmed.
0.41 Use of a FIDE title or rating to subvert the ethical principles of the title or rating system may subject a person to revocation of his title upon recommendation by the QC and the Ethics Commission and final action by the General Assembly.
0.42 A title is officially valid from the date all the requirements are met. In order for a title to be confirmed where it is based on an application, it must be published on the FIDE website and in other relevant FIDE documents for at least 60 days. For registered automatic titles see 0.5 below.
0.43 The title can be used for results of opponents only in tournaments starting after the confirmation. (exception: see 1.15)
0.44 In terms of, for example, the age of achieving a title, the title is considered to be achieved when the last result is achieved, and the rating requirement is fulfilled, whichever date is later.
0.5 Definitions
In the following text some special terms are used.
Rating performance is based on the player’s result and average rating of opponents (see 1.48).
Title performance (for example, GM performance) is a result that gives a performance rating as defined in 1.48 and 1.49 against the minimum average of the opponents, taking into account article 1.46, for that title. For example, for GM performance, average rating of the opponents ≥2380, and performance ≥2600, this might be achieved, for example, by a result of 7 points out of 9 games.
GM performance is ≥ 2600 performance against opponents with average rating ≥ 2380.
IM performance is ≥ 2450 performance against opponents with average rating ≥ 2230.
WGM performance is ≥ 2400 performance against opponents with average rating ≥ 2180.
WIM performance is ≥ 2250 performance against opponents with average rating ≥ 2030.
Title norm is a title performance fulfilling additional requirements concerning the mix of titled players and nationalities as specified in articles 1.42 to 1.47.
Direct title (automatic title) is a title gained by achieving a certain place or result in a tournament. For example, winning, or achieving a result ≥50 percent in a tournament. On application by the player’s federation and confirmation by the QC, such titles are awarded automatically by FIDE.


0.6 The Award of Titles
0.61 Titles may be awarded for specific results in specific Championship events, or are awarded on achieving a rating as laid down in these regulations. Such titles are confirmed by the QC Chairman on advice from the FIDE Office. They are then awarded by FIDE.

0.62 Titles are also awarded based on applications with norms with a sufficient number of games. These titles shall be awarded by the General Assembly on recommendation by the QC that the candidate meets the requirements. The Presidential Board or Executive Board may award titles in clear cases only, after consultation with the QC.

1 Requirements for Titles designated in 0.31
1.1 Administration
1.11 Play shall be governed by the FIDE Laws of Chess and FIDE Tournament Rules. Tournaments where the composition is changed (without QC approval) during the tournament or those where players have different conditions in terms of rounds and pairings are not valid. Unless with prior approval of the QC Chairman, the tournament must be registered at least 30 days in advance on the FIDE server.


1.12 There must be no more than 12 hours play in one day. This is calculated based on games that last 60 moves, although games played using delay or increment might last somewhat longer.
1.13 No more than 2 rounds shall be played on any one day. Without increment the minimum time is 2 hours for the first 40 moves followed by 30 minutes for the rest of the game. With an increment of a minimum of 30 seconds for each move, the minimum time is 90 minutes for the entire game, apart from the increment.
1.13a In the application for the GM title based on norms, at least one norm shall be achieved in a tournament with only one round per day for a minimum of 3 days.
1.13b In any title tournament the time controls and clock settings for all players must be the same (e.g. if the time control is increment based, all players must use increment; if delay based, all players must use delay; if no increment or delay is specified, then all players must compete with no increment and no delay). There can be no mixed use of clock settings (increment, delay, none at all).
1.14 Leagues and national team championships may last longer than 90 days, but not more than one year. Normally for individual tournaments, a period of at most 90 days is permitted but the QC Chairman may give prior approval to tournaments of a longer duration.






1.15 In tournaments which last longer than 90 days, the opponents’ ratings and titles used shall be those applying when the games were played.
1.16 The Chief Arbiter of a title tournament shall be an International Arbiter (IA) or FIDE Arbiter (FA). He may appoint a temporary deputy. An IA or FA must always be in the playing venue.



1.17 No arbiter may play in a title tournament even just as a filler.

1.2 Titles achieved from International Championships:
1.21 As indicated below, a player may gain a
(a) title from such an event or
(b) a single title norm. Then the requirements in 1.42 – 1.49 shall apply.
(c) a single title performance. Then the requirements in 1.42, 1.46 – 1.48 shall apply.
1.22 The minimum score is 35 % for all titles. The result shown is the minimum required.
1.23 For continental, sub-continental or approved competitions of FIDE International Affiliates, a title or result can be achieved if at least one third or three of the appropriate member federations – whichever is lower – participate in the event. The minimum number of participants in the event is eight. The World Championships (including U20) of the IBCA, ICSC and IPCA are exempted from this rule.
1.23a If groups are combined to make a bigger group, then the requirements (at least 8 participants from at least 3 federations) in 1.22 shall apply to this merged group. Titles can be awarded to the best player(s) of the subgroups, provided the subgroup has at least 5 participants from at least 2 federations and the player scores a minimum of 50% in a minimum of 9 games.
1.23b For Olympiad, a title norm counts as 20 games; a title performance counts as 13 games.
1.24 Terms used in Tables 1.24a and 1.24b: Gold – first after tiebreak; 1st equal – best 3 players after tiebreak; norm – 9 games (unless otherwise specified); Sub-Continentals – include Zonals, Subzonals, Arab, ASEAN and regional youth/school events; Each continent is allowed to designate a maximum of 3 regional youth/school events for direct titles
TABLE

1.3 Titles may be gained by achieving a published or interim rating at some time or other (see 1.53a):
1.31 FIDE Master ≥2300
1.32 Candidate Master ≥2200
1.33 oman FIDE Master ≥2100
1.34 Woman Candidate Master ≥2000
1.4 The GM, IM, WGM, WIM titles can also be gained by achieving norms in internationally rated tournaments played accordingly to the following regulations.
1.41 The Number of Games
1.41a The player must play at least 9 games, however
1.41b
Only 7 games are required for 7 round World Team and Continental Team Championships.
Only 7 games are required for 8 or 9 round World Team and Continental Team Championships.

Only 8 games are required for World Cup or Women’s World Championship Tournament where these 8-game norms count as 9 games.

1.41c For a 9 round tournament, if a player has just 8 games because of a forfeit or Bye, but he has met the correct mix of opponents in those games, then if he has a title result in 8 games, it counts as an 8 game norm.
1.41d Where a player exceeds the norm requirement by one or more full points, then these full points count as additional number of games when computing the total number of games for the norm achieved.
1.42 The following are not included.
1.42a Games against opponents who do not belong to FIDE federations.
1.42b Games against computers.
1.42c Games against unrated players who score zero against rated opponents in round robin tournaments.
1.42d Games which are decided by forfeit, adjudication or any means other than over the board play. Other games once started, which are forfeited for whatever reason, shall however be included. In the instance of a last round game where the opponent forfeits, the norm shall still count if the player must play in order to have the required number of games, but can afford to lose.

1.42e A player who has achieved a title result before the last round may ignore all games played subsequently, provided
(a) he has met the required mix of opponents.
(b) this leaves him with at least the minimum number of games as in 1.41.
(c) in the case of a tournament with pre-determined pairings, the full requirements, other than score, must be met for the complete tournament.
1.42f A player may ignore his game(s) against any opponent he has defeated, provided he has met the required mix of opponents, and provided that this leaves him with at least the minimum number of games as in 1.41 against the required mix of opponents. Nonetheless, the full cross-table of the tournament must be submitted.In the case of a tournament with pre-determined pairings, the full requirements, other than score, must be met for the complete tournament.
1.42g Tournaments that make changes to favour one or more players (for example by altering the number of rounds, or the order of rounds, or providing particular opponents, not otherwise participating in the tournament), shall be excluded.


1.43 Federations of Opponents
At least 2 federations other than that of the title applicant must be included, except 1.43a-1.43e shall be exempt. Nevertheless, 1.43f shall apply.
1.43a The final stage of the national men’s (or open) championship and also the national women’s championship. In the year when the Subzonal tournament of a single federation is held, then the national championship is not exempt for that federation.
1.43b National team championships.
1.43c Zonals and Subzonal tournaments.
1.43d Tournaments of other types may be included only with the prior approval of the QC Chairman.
1.43e Swiss System tournaments in which participants include at least 20 FIDE Rated players not from the host federation, but from at least 3 federations and at least 10 of whom hold GM, IM, WGM,WIM titles. Otherwise 1.44 applies.
1.43f At least one of the norms has to be achieved under normal foreigner requirement. (See 1.43 and 1.44).
1.44 Opponents shall be calculated using rounding up (minimum) to the next whole number, to the next lower number (maximum). A maximum of 3/5 of the opponents may come from the applicant’s federation and a maximum of 2/3 of the opponents from one federation. For exact numbers see the tables in 1.72.

1.45 Titles of Opponents – see 1.7 for exact numbers.
1.45a At least 50% of the opponents shall be title-holders (TH) as in 0.31, excluding CM and WCM.
1.45b For a GM norm, at least 1/3 with a minimum 3 of the opponents (MO) must be GMs.
1.45c or an IM norm, at least 1/3 with a minimum 3 of the opponents (MO) must be IMs or GMs.
1.45d For a WGM norm, at least 1/3 with a minimum 3 of the opponents (MO) must be WGMs, IMs or GMs.
1.45e For a WIM norm, at least 1/3 with a minimum 3 of the opponents (MO) must be WIMs, WGMs, IMs or GMs.
1.45f Double round robin tournaments need a minimum of 6 players. An opponent’s title as in 1.45b – 1.45e shall be counted only once.
1.46 Rating of Opponents
1.46a The Rating List in effect at the start of the tournament shall be used, see exception 1.15. The ratings of players who belong to federations which are temporarily excluded when the tournament starts can be determined on application to the FIDE Office.

1.46b For the purposes of norms, the minimum rating (adjusted rating floor) for the opponents shall be as follows:
Grandmaster 2200
International Master    2050
Woman Grandmaster   2000
Woman International Master  1850
1.46c No more than one opponent shall have his rating raised to this adjusted rating floor. Where more than one opponent is below the floor, the rating of the lowest opponent shall be raised.
1.46d Unrated opponents not covered by 1.46c shall be considered to be rated 1000.Minimum number of rated opponents, see table in 1.72. It can be calculated also so that maximum number of unrated opponents is 20 percent of (number of opponents+1).


1.47 Rating Average of Opponents
1.47a This is the total of the opponents’ ratings divided by the number of opponents taking 1.46c into consideration.
1.47b Rounding of the Rating Average is made to the nearest whole number. The fraction 0.5 is rounded upward.
1.48 Performance Rating (Rp)
In order to achieve a norm, a player must perform at a level of that shown below:
Minimum level prior to rounding Minimum level after rounding
GM 2599.5 2600
IM 2449.5 2450
WGM 2399.5 2400
WIM 2249.5 2250
Calculation of a Performance Rating (Rp):
Rp = Ra + dp (see the table below)
Where Ra = Average Rating of Opponents + Rating Difference ‘dp’ from Table 8.1a of FIDE Rating Regulations (conversion from percentage score ‘p’ into Rating Difference ‘dp’
1.48a The minimum average ratings Ra of the Opponents are as follows:
GM 2380; IM 2230; WGM 2180; WIM 2030.
1.49
p   dp  p   dp  p   dp  p   dp  p   dp  p   dp
1.0 800 .83 273 .66 117 .49 -7  .32  -133   .15 -296
.99 677 .82 262 .65 110 .48 -14 .31  -141   .14 -309
.98 589 .81 251 .64 102 .47 -21 .30  -149   .13 -322
.97 538 .80 240 .63 95  .46 -29 .29  -158   .12 -336
.96 501 .79 230 .62 87  .45 -36 .28  -166   .11 -351
.95 470 .78 220 .61 80  .44 -43 .27  -175   .10 -366
.94 444 .77 211 .60 72  .43 -50 .26  -184   .09 -383
.93 422 .76 202 .59 65  .42 -57 .25  -193   .08 -401
.92 401 .75 193 .58 57  .41 -65 .24  -202   .07 -422
.91 383 .74 184 .57 50  .40 -72 .23  -211   .06 -444
.90 366 .73 175 .56 43  .39 -80 .22  -220   .05 -470
.89 351 .72 166 .55 36  .38 -87 .21  -230   .04 -501
.88 336 .71 158 .54 29  .37 -95 .20  -240   .03 -538
.87 322 .70 149 .53 21  .36 -102    .19  -251   .02 -589
.86 309 .69 141 .52 14  .35 -110    .18  -262   .01 -677
.85 296 .68 133 .51 7   .34 -117    .17  -273   .00 -800
.84 284 .67 125 .50 0   .33 -125    .16  -284














1.5 Requirements for Award of the Title, having achieved Norms
1.51 Two or more norms in tournaments covering at least 27 games.
1.52 If a norm is sufficient for more than one title, then it may be used as part of the application for both.
1.53 To have achieved at some time or other a rating as follows:
GM≥2500
IM≥2400
WGM ≥2300
WIM≥2200

1.53a Such a rating need not be published. It can be obtained in the middle of a rating period, or even in the middle of a tournament. The player may then disregard subsequent results for the purpose of their title application. However, the burden of proof then rests with the federation of the title applicant. It is recommended that players receive a certificate from the Chief Arbiter where they achieve the rating level during a tournament. Such a certificate should include a note of the date each game was played. Title applications based on unpublished ratings shall only be accepted by FIDE after agreement with the Rating Administrator and the QC. Ratings in the middle of a period can be confirmed only after all tournaments for that period have been received and rated by FIDE.
1.54 A title result shall be valid if it was obtained in accordance with the FIDE Title Regulations prevailing at the time of the tournament when the norm was obtained.
1.55 Title norms gained before 1.7.2005 must be registered with FIDE before 1.7.2013 or they will be considered to have expired.
1.6 Summary of Title Tournament Requirements. In the case of any discrepancy, the regulations above shall take precedence. Notes
Number of Games per Day  Not more than 2 1.13
Rate of Play Minimum requirements        1.13
Period for the whole tournament Within 90 days, with exceptions 1.14
Administrator in charge International Arbiter or FIDE Arbiter 1.16
Number of Games Minimum 9 (7 in World/Continental Teams with 7-9 rounds) 1.41a-d
Type of Tournament No individual single matches 1.1
Games not Included
 - Against computers
 - Adjudicated games
 - Forfeited before play starts
 - Against opponents who do not belong to FIDE federations 1.42


1.61 For the numbers below, see the formula calculating titles in 1.45. Notes

Number of GMs, for GM MO 1/3 of opponents, minimum 3 GMs 1.45b
Number of IMs, for IM MO 1/3 of opponents, minimum 3 IMs 1.45c
Number of WGMs, for WGM MO 1/3 of opponents, minimum 3 WGMs 1.45d
Number of WIMs, for WIM MO 1/3 of opponents, minimum 3 WIMs 1.45e
Minimum Performance Rating
GM 2600; IM 2450;
WGM 2400; WIM 2250

1.48 Opponents’ minimum average rating
2380 for GM; 2230 for IM;
2180 for WGM; 2030 for WIM 1.7


Minimum score 35% 1.7


1.7 Summary of Requirements depending on the Number of Games
1.71 Determining whether a result is adequate for a norm, dependent on the average rating of the opponents. Tables 1.72 show the range for tournaments up to 19 rounds.Norms achieved in a tournament with more than 13 rounds count only as 13 games.
1.72

TABLES








1.8 Title Tournament Certificates
The Chief Arbiter must prepare in quadruplicate certificates of title results achieved. These copies must be provided to the player, the player’s federation, the organizing federation and FIDE. The player is recommended to ask the Chief Arbiter for the certificate before leaving the tournament. The Chief Arbiter is responsible that the TRF file must be submitted to FIDE.

1.9 Submission of Reports on Title Tournaments
Such tournaments must be registered as in 1.11.
1.91 The end of a tournament is the date of the last round and the deadline for submitting the tournament shall be calculated from that date.
1.92 The Chief Arbiter of a FIDE registered tournament has to provide the tournament report (TRF file) within 7 days after the end of the tournament to the Rating Officer of the federation where the tournament took place. The Rating Officer shall be responsible for uploading the TRF file to the FIDE Rating Server not later than 30 days after the end of the tournament.
1.93 Reports sent in more than 90 days late will not be accepted for rating or title purposes.
Table for Penalties for Late Submission of Tournament Reports
Type/Level of Tournament Within 30 days Within 60 days Within 90 days No Submission within 90 days
Swiss System – Individual and Team; Other Formats of Average Rating <2300 1 euro per player 100% Surcharge 200% Surcharge 300% Surcharge andsubject to investigation and recommendation of additional penalties by QC
Other Formats of Average Rating <2400 60 euro
Other Formats of Average Rating <2500 90 euro
Other Formats of Average Rating <2600 120 euro
Other Formats of Average Rating 2600 and > 150 euro
1.94 Reports shall include a database of at least those games played by players who achieved title results.

1.10 Application Procedure for Players’ Titles
1.10a Registration of Direct Titles
The Chief Arbiter sends the results to the FIDE Office. The FIDE Office together with the QC Chairman creates a list of possible titles. The federations concerned are informed by the FIDE Office. If the federation agrees to apply for the title, then the title is confirmed.
1.10b Titles by Application
The application must be sent and signed by the player’s federation. If the player’s federation refuses to apply, the player can appeal to FIDE and apply (and pay) for the title himself. All the certificates have to be signed by the Chief Arbiter and the federation responsible for the tournament.

2. Application Forms for titles are annexed hereto. They are:
Title Norm Forms Application Forms
Certificate of Title Result IT1 IT2
Tournament Report Form IT3












2.1 Applications for these titles must be prepared on these forms and all the information required supplied together with the application:
GM; IM; WGM; WIM - IT2, IT1s, each with cross-tables





2.2 Applications must be submitted to FIDE by the federation of the applicant. The national federation is responsible for the fee.
2.3 There is a 60-day deadline in order for the applications to be considered properly. There is a 50% surcharge for applications to be considered in a shorter time-scale than this. Those arriving during the Presidential Board, Executive Board or General Assembly shall be charged a 100% supplement.
Exception: the surcharge may be waived, if the last norm was achieved so late that the time limit could not be observed.
2.4 All applications together with full details must be posted on the FIDE website for a minimum of 60 days prior to finalisation. This is in order for any objections to be lodged.
3.0. List of Application Forms

    1. Certificate of title result IT1.
    2. Title Application form IT2.
    3. Tournament report form IT3.
